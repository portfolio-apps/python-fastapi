from fastapi import FastAPI
from src.items.ItemModel import ItemInput, Item, ResponseItemList, ResponseItem, DeleteSuccessResponse, items

import src.utils as utils

router = FastAPI()

######################################
##           API Routes
######################################
@router.get("/items", response_model=ResponseItemList, tags=["Items"])
async def list_items():
    return { 
        "success": True,
        "items": items 
    }

@router.post("/items", response_model=ResponseItem, tags=["Items"])
async def create_item(item: ItemInput):
    return {
        "success": True, 
        "item": item 
    }

@router.get("/items/{item_id}", response_model=ResponseItem, tags=["Items"])
async def read_item(item_id: int):
    return {
        "success": True, 
        "item": utils.find(items , item_id) 
    }

@router.put("/items/{item_id}", response_model=ResponseItem, tags=["Items"])
async def update_item(item_id: int, item: ItemInput):
    return { 
        "success": True,
        "item": utils.find(items , item_id)
    }

@router.delete("/items/{item_id}", response_model=DeleteSuccessResponse, tags=["Items"])
async def delete_item(item_id: int):
    return { "success": True }
