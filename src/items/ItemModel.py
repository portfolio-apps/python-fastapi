from typing import List
from pydantic import BaseModel, Field

class ItemInput(BaseModel):
    name: str
    age: int = Field(example=32)

class Item(BaseModel):
    id: int = Field(example=1)
    name: str = Field(example="Jerry")
    age: int = Field(example=32)

class ResponseItemList(BaseModel):
    success: bool = Field(example=True)
    items: List[Item]

class ResponseItem(BaseModel):
    success: bool = Field(example=True)
    item: Item

class DeleteSuccessResponse(BaseModel):
    success: bool = Field(example=True)

items = [
    {
        "id": 1,
        "name": "Harry",
        "age": 23
    },
    {
        "id": 2,
        "name": "Terry",
        "age": 22
    },
]