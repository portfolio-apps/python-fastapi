from fastapi import FastAPI
from fastapi.responses import HTMLResponse

from src.items.ItemRoutes import router

app = FastAPI()
app.mount("/api/v1", router)

######################################
##           Page Routes
######################################
@app.get("/", response_class=HTMLResponse, tags=["Pages"])
async def root():
    return """
    <html>
        <head>
            <title>FastAPI Boilerplate</title>
        </head>
        <body>
            <h1>Welcome to the FastAPI Boilerplate</h1>
            <br/>
            <a href="/about">About</a> <a href="/contact">Contact</a>
            <br/>
            <a href="/api/v1/docs">API Swagger Docs</a> <a href="/api/v1/redoc">API Redoc</a>
        </body>
    </html>
    """

@app.get("/about", response_class=HTMLResponse, tags=["Pages"])
async def about():
    return """
    <html>
        <head>
            <title>FastAPI Boilerplate</title>
        </head>
        <body>
            <h1>Welcome to the FastAPI Boilerplate - About Page</h1>
            <br/>
            <a href="/">Home</a>
        </body>
    </html>
    """

@app.get("/contact", response_class=HTMLResponse, tags=["Pages"])
async def contact():
    return """
    <html>
        <head>
            <title>FastAPI Boilerplate</title>
        </head>
        <body>
            <h1>Welcome to the FastAPI Boilerplate - Contact Page</h1>
            <br/>
            <a href="/">Home</a>
        </body>
    </html>
    """